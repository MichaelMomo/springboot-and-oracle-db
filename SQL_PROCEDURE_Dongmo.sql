--------------------------------------------------------------------------------
/* Esercizio 1
   Procedura con esempi base di LOOP, WHILE e FOR
*/

CREATE OR REPLACE PROCEDURE P_LOOP IS
    i NUMBER;
BEGIN
    i := 1;
	
--  stampo la stringa 'Tre tipi di loop da 1 a 20'
    dbms_output.put_line('Tre tipi di loop da 1 a 20');
    LOOP
        dbms_output.put_line('LOOP BASE ' || TO_CHAR(i));
        i := i + 1;
        EXIT WHEN i > 20;
    END LOOP;

    i := 1;

--  stampo la parola WHILE 20 volte
    dbms_output.put_line('WHILE ' || TO_CHAR(i));
    WHILE i <= 20 LOOP i := i + 1;
    END LOOP;

--  stampo la parola FOR 20 volte
    dbms_output.put_line('FOR ' || TO_CHAR(i));
    FOR i IN 1..20 LOOP i := i + 1;
    END LOOP;

--  eccezione
EXCEPTION
    WHEN no_data_found THEN
        NULL;
END;

--------------------------------------------------------------------------------
/* Esercizio 2
   Utilizzo del comando WHILE
*/

-- procedura che crea dinamicamente la tabella T1 che servirà per l'esercizio sotto.
CREATE OR REPLACE PROCEDURE p_create_t1 AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'create table T1 (col1 number)';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T1 creata');
END;

--procedura che scrive una sequenza di numeri interi da 1 a 5 utilizzando la struttura WHILE
CREATE OR REPLACE PROCEDURE p_while AS
    i NUMBER(10) := 1;
BEGIN
    WHILE i <= 5 LOOP
        INSERT INTO t1 ( col1 ) VALUES ( i );
        i := i + 1;
    END LOOP;

    dbms_output.put_line('Inseriti valori nella tabella T1');
    COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 3
   Associare la giusta stringa 'PARI' o 'DISPARI' agli interi compresi tra 1 e 10. 
*/

--procedura che crea dinamicamente la tabella T2
CREATE OR REPLACE PROCEDURE p_create_t2 AS
    stmt VARCHAR(100);
BEGIN
    stmt := 'create table T2 (NUMERO number(10), MESSAGE varchar2(10))';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T2 creata');
END;

--procedura che assegna la giusta stringa 'PARI' o 'DISPARI'.
CREATE OR REPLACE PROCEDURE p_par_disp AS
    i   NUMBER(10) := 1;
    s   VARCHAR2(10);
BEGIN
    WHILE i <= 10 LOOP
        IF ( i MOD 2 ) = 0 THEN
            s := 'PARI';
        ELSE
            s := 'DISPARI';
        END IF;

        INSERT INTO t2 (
            numero,
            message
        ) VALUES (
            i,
            s
        );

        i := i + 1;
    END LOOP;

    dbms_output.put_line('Inseriti valori nella tabella T2');
    COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 4
   Calcolare il fattoriale del numero 5 mediante l'uso del ciclo While.
*/

--procedura che crea dinamicamente la tabella T3
CREATE OR REPLACE PROCEDURE p_create_t3 AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'create table T3(N1 number(10),N2 number(10))';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T3 creata');
END;

--procedura che calcola il fattoriale del numero 5
CREATE OR REPLACE PROCEDURE p_fattoriale AS
    i   NUMBER(10) := 5;
    f   NUMBER(10) := 5;
BEGIN
    WHILE i > 2 LOOP
        f := f * ( i - 1 );
        i := i - 1;
    END LOOP;

    INSERT INTO t3 (
        n1,
        n2
    ) VALUES (
        5,
        f
    );

    dbms_output.put_line('Inseriti valori nella tabella T3');
    COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 5
   Calcolare il fattoriale del numero 5 mediante l'uso del ciclo For.
*/

--Usiamo la stessa tabella dell'esercizio 4. Creiamo una procedura che svuota la tabella.
CREATE OR REPLACE PROCEDURE truncate_t3 AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'Truncate table T3';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T3 troncata');
END;

--procedura che calcola il fattoriale del numero 5.
CREATE OR REPLACE PROCEDURE p_fatt_for AS
    i   NUMBER(10) := 5;
    f   NUMBER(10) := 5;
BEGIN
    FOR i IN REVERSE 2..5 LOOP f := f * ( i - 1 );
    END LOOP;

    INSERT INTO t3 (
        n1,
        n2
    ) VALUES (
        5,
        f
    );

    dbms_output.put_line('Inseriti valori nella tabella T3');
    COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 6
   Calcolare il fattoriale dei numeri compresi tra 1 e 10.
*/

--Usiamo la stessa tabella dell'esercizio 4. Uilizziamo la procedura che svuota la tabella.
EXEC truncate_t3;

--procedura che calcola il fattoriale dei numeri compresi tra 1 e 10
CREATE OR REPLACE PROCEDURE p_fatt AS
    i   NUMBER(10);
    x   NUMBER(10);
    f   NUMBER(10) := 5;
BEGIN
    FOR x IN 1..10 LOOP
        f := x;
        IF f > 1 THEN
            FOR i IN REVERSE 2..x LOOP
                f := f * ( i - 1 );
            END LOOP;

        END IF;

        INSERT INTO t3 (
            n1,
            n2
        ) VALUES (
            x,
            f
        );

    END LOOP;

    dbms_output.put_line('Inseriti valori nella tabella T3');
    COMMIT;
END;

--------------------------------------------------------------------------------
/*	Esercizio 7
    Selezionare i 5 dipendenti meno pagati ed inserire il loro nome,
	la matricola e lo stipendio nella tabella T6. 
*/

--procedura che crea dinamicamente la tabella T6
CREATE OR REPLACE PROCEDURE p_create_t6 AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'create table T6(matricola number(6), nome varchar2(20), cognome varchar2(20), stipendio number(8,2)';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T6 creata');
END;

--procedura che seleziona i 5 dipendenti meno pagati e li inserisce nella tabella t6.
CREATE OR REPLACE PROCEDURE p_emp AS
    i NUMERIC(10) := 0;
BEGIN
    FOR r IN (
        SELECT
            employee_id,
            first_name,
            last_name,
            salary
        FROM
            employees
        ORDER BY
            salary
    ) LOOP
        i := i + 1;
        IF i <= 5 THEN
            INSERT INTO t6 (
                matricola,
                nome,
                cognome,
                stipendio
            ) VALUES (
                r.employee_id,
                r.first_name,
                r.last_name,
                r.salary
            );

        ELSE
            EXIT;
        END IF;

    END LOOP;

    dbms_output.put_line('Inseriti valori nella tabella T6');
    COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 8
   Come esercizio 7 ma con cursore esplicito
*/

--procedura che tronca la tabella T6
CREATE OR REPLACE PROCEDURE truncate_t6 AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'truncate table T6';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T6 troncata');
END;

----procedura che seleziona i 5 dipendenti meno pagati e li inserisce nella tabella t6 con cursore esplicito.
CREATE OR REPLACE PROCEDURE p_emp_curs AS

    CURSOR c1 IS
    SELECT
        employee_id,
        first_name,
        last_name,
        salary
    FROM
        employees
    ORDER BY
        salary;

    rec1 c1%rowtype;
BEGIN
    OPEN c1;
    FOR i IN 1..5 LOOP
        FETCH c1 INTO rec1;
        EXIT WHEN c1%notfound;
        INSERT INTO t6 (
            matricola,
            nome,
            cognome,
            stipendio
        ) VALUES (
            rec1.employee_id,
            rec1.first_name,
            rec1.last_name,
            rec1.salary
        );

    END LOOP;

    CLOSE c1;
    dbms_output.put_line('Inseriti valori nella tabella T6');
    COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 9
   Aumentare il valore della colonna SALARY della tavola EMPLOYEES ai dipendenti selezionati; 
   l'aumento deve essere del 20% se guadagnano meno di 1999 e del 10% se guadagnano di più. 
*/

--Al posto di modificare employees creiamo una copia identica e lavoreremo su quella.
CREATE OR REPLACE PROCEDURE p_copy_emp AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'create table T7 as select * from employees';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T7 copiata');
END;

CREATE OR REPLACE PROCEDURE p_upd_sal AS
    CURSOR c1 IS
    SELECT
        employee_id,
        salary
    FROM
        employees;

    rec1 c1%rowtype;
BEGIN
    OPEN c1;
    LOOP
        FETCH c1 INTO rec1;
        EXIT WHEN c1%notfound;
        IF rec1.salary < 1999 THEN
            UPDATE t7
            SET
                salary = salary * 1.2
            WHERE
                employee_id = rec1.employee_id;

        ELSE
            UPDATE t7
            SET
                salary = salary * 1.1
            WHERE
                employee_id = rec1.employee_id;

        END IF;

    END LOOP;

    CLOSE c1;
    dbms_output.put_line('Tabella T7 aggiornata');
	
    COMMIT;
	
END;

--------------------------------------------------------------------------------
/* Esercizio 10
   Calcolare per ogni impiegato la percentuale dello stipendio rispetto al totale generale.
*/

--procedura che crea dinamicamente la tabella T8.
CREATE OR REPLACE PROCEDURE p_create_t8 AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'create table T8 (nome varchar2(20), cognome varchar2(25), stipendio number(8,2), percentuale number(5,2))';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T8 creata');
END;

--procedura che calcola la percentuale dello stipendio dell'impiegato.
CREATE OR REPLACE PROCEDURE p_percent AS

    CURSOR c1 IS
    SELECT
        employee_id,
        first_name,
        last_name,
        salary
    FROM
        employees;

    rec1     c1%rowtype;
    totale   NUMBER(10);
    
BEGIN
    SELECT
        SUM(salary)
    INTO totale
    FROM
        employees;

    OPEN c1;
    LOOP
        FETCH c1 INTO rec1;
        EXIT WHEN c1%notfound;
        INSERT INTO t8 VALUES (
            rec1.first_name,
            rec1.last_name,
            rec1.salary,
            ( rec1.salary / totale ) * 100
        );

    END LOOP;

    CLOSE c1;
    dbms_output.put_line('Inseriti valori nella tabella T8');
  COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 11
   Calcolare per ogni impiegato la percentuale dello stipendio rispetto al proprio dipartimento. 
*/

--procedura che crea dinamicamente la tabella T9.
CREATE OR REPLACE PROCEDURE p_create_t9 AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'create table T9 (pid number(4),cognome varchar2(25), stipendio number(8,2), percentuale number(5,2))';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T9 creata');
END;

--procedura che calcola la percentuale dello stipendio per ogni impiegato.
CREATE OR REPLACE PROCEDURE p_perc_sal AS
    salarydiptot   NUMBER(10, 2) := 0;
    departmentid   NUMBER(4);
BEGIN
    SELECT
        MAX(department_id) + 10
    INTO departmentid
    FROM
        employees;

    FOR r IN (
        SELECT
            employee_id,
            last_name,
            salary,
            nvl(department_id, 0) department_id
        FROM
            employees
        ORDER BY
            nvl(department_id, 0)
    ) LOOP
        IF departmentid != r.department_id THEN
            departmentid := r.department_id;
            SELECT
                SUM(salary)
            INTO salarydiptot
            FROM
                employees
            WHERE
                nvl(department_id, 0) = departmentid;

        END IF;

        INSERT INTO t9 VALUES (
            r.employee_id,
            r.last_name,
            r.salary,
            r.salary / salarydiptot * 100
        );

    END LOOP;

    dbms_output.put_line('Inseriti valori nella tabella T9');
    COMMIT;
    
END;

--------------------------------------------------------------------------------
/* Esercizio 12
   Per ogni dipartimento calcolare la percentuale della somma degli stipendi rispetto al totale generale.
*/

--procedura che crea dinamicamente la tabella T10
CREATE OR REPLACE PROCEDURE p_create_t10 AS
    stmt VARCHAR2(200);
BEGIN
    stmt := 'create table T10 (pid number(4), nomeDipartimento varchar2(25), stipendiDipartimento number(8), percentuale number(5,2))'
    ;
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T10 creata');
END;

--procedura che calcola la percentuae della somma degli stipendi.
CREATE OR REPLACE PROCEDURE p_somma_perc AS
    salarytot NUMBER(10, 2);
BEGIN
    SELECT
        SUM(salary)
    INTO salarytot
    FROM
        employees;

    FOR r IN (
        SELECT
            d.department_id,
            SUM(nvl(e.salary, 0)) salarydip,
            d.department_name
        FROM
            departments   d,
            employees     e
        WHERE
            d.department_id = e.department_id
        GROUP BY
            d.department_id,
            d.department_name
    ) LOOP INSERT INTO t10 VALUES (
        r.department_id,
        r.department_name,
        r.salarydip,
        r.salarydip / salarytot * 100
    );

    END LOOP;

    dbms_output.put_line('Inseriti valori nella tabella T10');
    
    COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 13
   Calcolare in numero degli impiegati in dipartimenti con varie condizioni di WHERE.
*/

--procedura che crea dinamicamente la tabella T11.
CREATE OR REPLACE PROCEDURE p_create_t11 AS
    stmt VARCHAR2(200);
BEGIN
    stmt := 'create table T11 (pid number(4), N1 number(3), N2 number(3), N3 number(3), N4 number(3), N5 number(3), N6 number(3))'
    ;
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T11 creata');
END;

--procedura che conta il numero degli impiegati in determinati dipartimenti.
CREATE OR REPLACE PROCEDURE p_select_count AS
    n1   NUMBER(4);
    n2   NUMBER(4);
    n3   NUMBER(4);
    n4   NUMBER(4);
    n5   NUMBER(4);
    n6   NUMBER(4);
BEGIN
    FOR r IN (
        SELECT DISTINCT
            department_id
        FROM
            employees
        ORDER BY
            1
    ) LOOP
        SELECT
            COUNT(*)
        INTO n1
        FROM
            employees
        WHERE
            department_id = r.department_id;

        SELECT
            COUNT(*)
        INTO n2
        FROM
            employees
        WHERE
            department_id = r.department_id
            AND job_id = 'IT_PROG';

        SELECT
            COUNT(*)
        INTO n3
        FROM
            employees
        WHERE
            department_id = r.department_id
            AND ( job_id LIKE '%MGR'
                  OR job_id LIKE '%MAN' );

        SELECT
            COUNT(*)
        INTO n4
        FROM
            employees
        WHERE
            department_id = r.department_id
            AND job_id LIKE '%CLERK';

        SELECT
            COUNT(*)
        INTO n5
        FROM
            employees
        WHERE
            department_id = r.department_id
            AND ( job_id = 'SA_REP'
                  OR job_id = 'MK_REP' );

        SELECT
            COUNT(*)
        INTO n6
        FROM
            employees
        WHERE
            department_id = r.department_id
            AND job_id NOT IN (
                'IT_PROG',
                'SA_REP',
                'MK_REP'
            )
            AND job_id NOT LIKE '%MGR'
            AND job_id NOT LIKE '%MAN'
            AND job_id NOT LIKE '%CLERK';

        INSERT INTO t11 VALUES (
            r.department_id,
            n1,
            n2,
            n3,
            n4,
            n5,
            n6
        );

    END LOOP;

    dbms_output.put_line('Inseriti valori nella tabella T11');
    COMMIT;
END;

--------------------------------------------------------------------------------
/* Esercizio 14
   Selezionare il numero dell'impiegato, il suo cognome ed inserirli nella tabella attraverso un LOOP.
   Selezionare il numero del dipartimento ed inserirlo nella tabella attraverso un LOOP.
*/

--procedura che crea dinamicamente la tabella T16
CREATE OR REPLACE PROCEDURE p_create_t16 AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'create table T16 (pid_dip number(4))';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T16 creata');
END;

--procedura che crea dinamicamente la tabella IMP
CREATE OR REPLACE PROCEDURE p_create_imp AS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'create table IMP (id_imp number(4), nome_imp varchar2(20))';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella IMP creata');
END;

--procedura che inserisce valori nelle tabelle estratti dal LOOP
CREATE OR REPLACE PROCEDURE p_for_loop AS
    e_commissione EXCEPTION;
    impnonscad NUMBER(10) := 0;
BEGIN
    FOR r IN (
        SELECT
            employee_id,
            last_name
        FROM
            employees
        ORDER BY
            1
    ) LOOP INSERT INTO imp (
        id_imp,
        nome_imp
    ) VALUES (
        r.employee_id,
        r.last_name
    );

    END LOOP;

    FOR r IN (
        SELECT DISTINCT
            department_id
        FROM
            employees
        ORDER BY
            1
    ) LOOP INSERT INTO t16 ( pid_dip ) VALUES ( r.department_id );

    END LOOP;

    COMMIT;
    dbms_output.put_line('Inseriti valori nelle tabelle T16 e IMP');
END;

--------------------------------------------------------------------------------
/* Esercizio 15
   Assegnamento variabili, costrutto IF THEN ELSE e controllo valori nulli
*/

CREATE OR REPLACE PROCEDURE p_variables IS
    i     NUMBER;     -- variabile in condizione if (pari o dispari)
    j     NUMBER := 1; -- controllo cicli
    lim   NUMBER := 2; -- limite superiore cicli
BEGIN
    i := 54;
    SELECT
        13
    INTO i         -- assegnameto con select
    FROM
        dual;

    IF nvl(i, 0) = 0 THEN    -- controllo se i è null
        dbms_output.put_line('var i NULL value');
    ELSIF MOD(i, 2) = 0 THEN
        WHILE j < lim LOOP
            dbms_output.put_line('while executed, i = '
                                 || TO_CHAR(i)
                                 || ' is even');
            j := j + 1;
        END LOOP;
    ELSE
        FOR j IN 1..( lim - 1 ) LOOP
            dbms_output.put_line('for executed, i = '
                                 || TO_CHAR(i)
                                 || ' is odd');
        END LOOP;
    END IF;

EXCEPTION
    WHEN no_data_found THEN
        NULL;
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
        RAISE;
END;

-- come p_variables, ma la variabile i e' passata come parametro
CREATE OR REPLACE PROCEDURE p_parametro (
    i NUMBER
) IS
    j     NUMBER := 1; -- controllo cicli
    lim   NUMBER := 2; -- limite superiore cicli
BEGIN
    IF nvl(i, 0) = 0 THEN    -- controllo se i è null
        dbms_output.put_line('var i NULL value');
    ELSIF MOD(i, 2) = 0 THEN
        WHILE j < lim LOOP
            dbms_output.put_line('while executed, i = '
                                 || TO_CHAR(i)
                                 || ' is even');
            j := j + 1;
        END LOOP;
    ELSE
        FOR j IN 1..( lim - 1 ) LOOP
            dbms_output.put_line('for executed, i = '
                                 || TO_CHAR(i)
                                 || ' is odd');
        END LOOP;
    END IF;
EXCEPTION
    WHEN no_data_found THEN
        NULL;
    WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
        RAISE;
END;
-------------------------------------------------------------------------------
-- Blocco test procedura p_parametro
-------------------------------------------------------------------------------
--BEGIN 
--    p_parametro(44);
--    p_parametro(13);
--    p_parametro(NULL);
--END;

-------------------------------------------------------------------------------
/* Esercizio 16
   Come procedura P_LOOP , ma inserisce l'output anche nella tabella T1I, oltre che stampare su video
*/

--procedura che crea dinamicamente la tabella T1I
CREATE OR REPLACE PROCEDURE p_create_t1i AS
    stmt VARCHAR2(200);
BEGIN
    stmt := 'create table T1I (IDT1I number, PN varchar2(20), DESCR varchar2(20), DINS date)';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T1I creata');
END;

--creazione sequenza MYSEQ   
CREATE SEQUENCE myseq START WITH 1 INCREMENT BY 1;

--procedura che tronca la tabella T1I
CREATE OR REPLACE PROCEDURE truncate_t1i IS
    stmt VARCHAR2(100);
BEGIN
    stmt := 'truncate table T1I';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella T1I troncata');
END;

-- inserisce in t1i
CREATE OR REPLACE PROCEDURE ins_t1i (
    idi      NUMBER,
    pni      VARCHAR2,
    descri   VARCHAR2,
    dinsi    DATE
) IS
BEGIN
    INSERT INTO t1i (
        idt1i,
        pn,
        descr,
        dins
    ) VALUES (
        idi,
        pni,
        descri,
        dinsi
    );
END;

--inserisco 10.000 record nella tabella T1I ed ogni 1000 eseguo commit
CREATE OR REPLACE PROCEDURE insert_loop IS
   tot         NUMBER;
   commit_on   NUMBER;
   i           NUMBER;
BEGIN
   tot := 10000;
   commit_on := 1000;
   i := 1;
   
   WHILE i <= tot LOOP
       ins_t1i(myseq.nextval - 1, 'insert_loop', 'looping ' || i, SYSDATE);
       IF MOD(i, commit_on) = 0 THEN
           COMMIT;
       END IF;
       i := i + 1;
   END LOOP;

END;

--procedura chiamante
CREATE OR REPLACE PROCEDURE p_loop_seq IS
    i NUMBER;
BEGIN
    i := 1;
    LOOP
        dbms_output.put_line('LOOP ' || TO_CHAR(i));
        ins_t1i(myseq.nextval, 'P1I', 'LOOP ' || TO_CHAR(i), SYSDATE);

        i := i + 1;
        EXIT WHEN i > 20;
    END LOOP;

    i := 1;
    WHILE i <= 20 LOOP
        dbms_output.put_line('WHILE ' || TO_CHAR(i));
        ins_t1i(myseq.nextval, 'P1I', 'LOOP ' || TO_CHAR(i), SYSDATE);

        i := i + 1;
    END LOOP;

    FOR i IN 1..20 LOOP
        dbms_output.put_line('FOR ' || TO_CHAR(i));
        ins_t1i(myseq.nextval, 'P1I', 'LOOP ' || TO_CHAR(i), SYSDATE);

    END LOOP;

EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
END;

--------------------------------------------------------------------------------
/* Esercizio 17
   Gestione eccezioni.
   Ogni log generato va su output video e nella tabella TLOG_ERROR.
*/

--procedura che crea dinamicamente la tabella TLOG_ERROR
CREATE OR REPLACE PROCEDURE p_create_log_error IS
    stmt VARCHAR2(200);
BEGIN
    stmt := 'create table TLOG_ERROR (CALLER varchar2(30), ORCL_CODE number, ORCL_MSG varchar2(110), TIME_STAMP date)';
    EXECUTE IMMEDIATE stmt;
    dbms_output.put_line('Tabella TLOG_ERROR creata');
END;

--procedura su molteplici eccezioni
CREATE OR REPLACE PROCEDURE p_error_log IS
    i       NUMBER;
    v_msg   VARCHAR2(30);
    v_cod   NUMBER;
BEGIN
    BEGIN                            -- NO_DATA_FOUND Block
        SELECT
            idt1i
        INTO i
        FROM
            t1i
        WHERE
            idt1i = 1;

    EXCEPTION
        WHEN no_data_found THEN
            i := 1;
            dbms_output.put_line('P_ERROR_LOG: '
                                 || TO_CHAR(sqlcode)
                                 || ': '
                                 || substr(sqlerrm, 1, 100));

            dbms_output.put_line('generated on: ' || SYSDATE);
            v_msg := substr(sqlerrm, 1, 100);
            v_cod := sqlcode;
            INSERT INTO tlog_error (
                caller,
                orcl_code,
                orcl_msg,
                time_stamp
            ) VALUES (
                'P_ERROR_LOG',
                v_cod,
                v_msg,
                SYSDATE
            );

    END;

    BEGIN                            -- TOO_MANY_ROWS Block
        SELECT
            idt1i
        INTO i
        FROM
            t1i
        WHERE
            idt1i > 1;

    EXCEPTION
        WHEN too_many_rows THEN
            i := 1;
            dbms_output.put_line('P2I: '
                                 || TO_CHAR(sqlcode)
                                 || ': '
                                 || substr(sqlerrm, 1, 100));

            dbms_output.put_line('generated on: ' || SYSDATE);
            v_msg := substr(sqlerrm, 1, 100);
            v_cod := sqlcode;
            INSERT INTO tlog_error (
                caller,
                orcl_code,
                orcl_msg,
                time_stamp
            ) VALUES (
                'P_ERROR_LOG',
                v_cod,
                v_msg,
                SYSDATE
            );

    END;

    BEGIN                            --  ZERO_DIVIDE Block
        i := 1 / 0;
    EXCEPTION
        WHEN zero_divide THEN
            i := 1;
            dbms_output.put_line('P_ERROR_LOG: '
                                 || TO_CHAR(sqlcode)
                                 || ': '
                                 || substr(sqlerrm, 1, 100));

            dbms_output.put_line('generated on: ' || SYSDATE);
            v_msg := substr(sqlerrm, 1, 100);
            v_cod := sqlcode;
            INSERT INTO tlog_error (
                caller,
                orcl_code,
                orcl_msg,
                time_stamp
            ) VALUES (
                'P_ERROR_LOG',
                v_cod,
                v_msg,
                SYSDATE
            );

    END;

EXCEPTION                           -- Caso generico errore 
    WHEN OTHERS THEN
        dbms_output.put_line('P_ERROR_LOG: '
                             || TO_CHAR(sqlcode)
                             || ': '
                             || substr(sqlerrm, 1, 100));

        dbms_output.put_line('generated on: ' || SYSDATE);
        INSERT INTO tlog_error (
            caller,
            orcl_code,
            orcl_msg,
            time_stamp
        ) VALUES (
            'P2_ERROR_LOG',
            v_cod,
            v_msg,
            SYSDATE
        );

END;

--------------------------------------------------------------------------------
/* Esercizio 18
   Uso di commandi DDL all'interno di procedure.
   P_CREATE_OR_DROP: cerca la tabella di nome TT1I,
   se non esiste la crea con 1 sola colonna eta di tipo number,  
   altrimenti la cancella e la ricrea. 
*/

CREATE OR REPLACE PROCEDURE p_create_or_drop IS
    v_count   NUMBER;
    v_msg     VARCHAR2(30);
    v_cod     NUMBER;
BEGIN
    SELECT
        COUNT(*)
    INTO v_count
    FROM
        user_catalog
    WHERE
        table_name = 'TT1I';

    IF v_count = 0 THEN
        dbms_output.put_line('Tabella TT1I non trovata');
    ELSE
        dbms_output.put_line('Tabella TT1I trovata');
        EXECUTE IMMEDIATE 'drop table TT1I';
    END IF;

    EXECUTE IMMEDIATE 'CREATE TABLE '
                      || 'TT1I'
                      || '(
         eta NUMBER NOT NULL)';
EXCEPTION                         -- Caso generico errore 
    WHEN OTHERS THEN
        dbms_output.put_line('P3I: '
                             || TO_CHAR(sqlcode)
                             || ': '
                             || substr(sqlerrm, 1, 100));

        dbms_output.put_line('generated on: ' || SYSDATE);
        INSERT INTO tlog_error (
            caller,
            orcl_code,
            orcl_msg,
            time_stamp
        ) VALUES (
            'P_ERROR_LOG',
            v_cod,
            v_msg,
            SYSDATE
        );

        ROLLBACK;
END;