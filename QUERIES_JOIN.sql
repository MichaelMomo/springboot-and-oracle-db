--MASSIMO GUARDAGNO 

file://192.168.1.254/Kingston_DataTraveler30_1_3682/04%20Mike/

create or replace FUNCTION f_max_guadagno (
    p_classe_di_rischio NUMBER,
    p_capitale_da_investire NUMBER
) RETURN NUMBER IS
   /*
   Avendo nel DB una funzione Java restituisci prossimo titolo che implementa l'interfaccia indicata nel testo dell'esercizio,
si può richiamare direttamente tale funzione al posto di scorrere con il cursore cur_int la tabella ottenuta dalla join
delle tabelle INTERESSI, CLASSI DI RISCHIO e TITOLI.

   */

    CURSOR cur_tit (
        p_classe classi_di_rischio.id_rischio%TYPE
    ) 
-- Cursore parametro che scorre solo le righe relative  alla classe di rischio --- inserita
     IS
    SELECT
        t.id_titolo,
        t.quantita,
        t.costo,
        c.id_rischio,
        c.durata,
        c.max_investibile,
        i.interesse
    FROM
        interessi           i,
        classi_di_rischio   c,
        titoli              t
    WHERE
        i.id_titolo = t.id_titolo
        AND i.id_rischio = c.id_rischio
        AND i.id_rischio = p_classe
    ORDER BY
        i.interesse DESC;

    row_tit                cur_tit%rowtype;
    v_massimo_guadagno     NUMBER := 0;
-- Capitale investibile per titolo
    v_capitale_utile       classi_di_rischio.max_investibile%TYPE;
-- Capitale residuo in seguito ai vari investimenti
    v_capitale_rimanente   classi_di_rischio.max_investibile%TYPE;
    v_num_azioni           titoli.quantita%TYPE;
    exc_capitale_negativo EXCEPTION;
BEGIN
    OPEN cur_tit(p_classe_di_rischio);
    v_capitale_rimanente := p_capitale_da_investire;
    IF ( p_capitale_da_investire > 0 ) THEN
        LOOP
            FETCH cur_tit INTO row_tit;
--Il cursore è vuoto se la classe di rischio inserita non è presente
            IF ( row_tit.id_titolo IS NULL ) THEN
                dbms_output.put_line('La classe di rischio inserita non è conforme');
            END IF;

            EXIT WHEN ( cur_tit%notfound OR ( v_capitale_rimanente = 0 ) );
--Si considera il minimo tra il capitale che si può investire sul titolo in     --esame e il capitale a disposizione
            v_capitale_utile := least(v_capitale_rimanente, row_tit.max_investibile);
--Si considera la funzione trunc per avere il massimo numero intero di azioni   --acquistabili con il capitale a disposizione
            v_num_azioni := least(row_tit.quantita, trunc(v_capitale_utile / row_tit.costo));

            IF ( v_num_azioni != 0 ) THEN
--Si sottrae al capitale la spesa sostenuta per l'investimento
                v_capitale_rimanente := v_capitale_rimanente - row_tit.costo * v_num_azioni;
--Si applica la formula data
                v_massimo_guadagno := v_massimo_guadagno + ( v_num_azioni * row_tit.costo * row_tit.interesse * row_tit.durata / 12
                );

            END IF;

        END LOOP;

        CLOSE cur_tit;

--Si considera l'eventuale residuo agli investimenti eseguiti dovuto ai limiti ---di investimento
        v_massimo_guadagno := v_massimo_guadagno + v_capitale_rimanente;
    ELSE
--Si scatena l'eccezione in caso di capitale di investimento inserito negativo
        RAISE exc_capitale_negativo;
    END IF;

    RETURN v_massimo_guadagno;
EXCEPTION
    WHEN exc_capitale_negativo THEN
        dbms_output.put_line('Il capitale da investire è un valore negativo');
        RETURN 0;
END f_max_guadagno;
/

--FUNZIONE DI MAURO

select to_char(phone_number) as c1
        ,to_char(hire_date,'HH24:MI:SS') as c11
        ,to_number(to_char(hire_date,'MM')) as c111
        ,to_date('10/07/2017','DD/MM/YYYY') as c2
        ,to_number('12345') as c3
        ,nvl(commission_pct, 0) as c4
        ,nvl2(commission_pct, 1, 0) as c5
        ,first_name
        ,case when upper(first_name) like '%A'
        then 'Femmina' else 'Maschio' end as c6
        ,case   job_id
                when 'AD_PRES' then 'Presidente'
                when 'AD_VP' then 'Vice Presidente'
                when 'IT_PROG' then 'Programmatore'
                else 'Altro impiego' end as c66
        ,decode (job_id, 'IT_PROG', 'Meglio dei sistemisti', 'Impiegato') as c7
        ,decode (job_id, 'IT_PROG', 'Meglio dei sistemisti') as c77
        ,coalesce(null,1,2) as c8
        ,coalesce(null,null,2) as c88
        ,coalesce(commission_pct, department_id) as c888
        ,substr(last_name,1,3) as c9
        ,length(first_name) as c10
        ,employee_id
        ,mod(employee_id,2) as c1111
        ,upper(instr(upper(first_name), 'A', 1, 1)) as c12
        ,trim  ('       ciao    ') as c13
        ,ltrim ('       ciao   ') as c14
        ,rtrim ('       ciao   ') as c15
        ,job_id
        ,salary
        -- funzioni alnalitiche
        ,trunc(avg(salary) over (partition by job_id)) as c16
        ,trunc(max(salary) over (partition by job_id)) as c166
        ,trunc(min(salary) over (partition by job_id)) as c1666
        ,trunc(count(salary) over (partition by job_id)) as c16666
        ,rank () over (partition by job_id order by salary) as c17
        ,rank () over (order by salary desc) as c177
        ,row_number() over (order by salary desc) as c18
        ,hire_date
        ,FIRST_VALUE(hire_date) OVER (ORDER BY salary desc) AS c19
        ,LAST_VALUE(hire_date) OVER (ORDER BY salary desc) AS c20
        ,salary as sal
        ,LAG(salary, 2, 0) OVER (ORDER BY hire_date desc) AS c21
        ,Lead(salary, 1, 0) OVER (ORDER BY hire_date desc) AS c22
from employees
order by salary;
/

--create table 

CREATE TABLE t_debug (
id_t_debug  NUMBER(5),
timestamp_debug   DATE,
description_debug VARCHAR2(20))
PCTFREE 0;
/

-- create a sequence 

CREATE SEQUENCE t_debug_seq
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 20;
/

--create procedure 

create or replace PROCEDURE p_debug(
    id_t_debug        NUMBER,
    timestamp_debug   DATE,
    description_debug VARCHAR2
)IS
 
BEGIN
    INSERT INTO t_debug VALUES (id_t_debug, timestamp_debug, description_debug);
    COMMIT;
END;
/

SELECT
    *
FROM t_debug;
/

CREATE OR REPLACE VIEW V_T_DEBUG AS 
SELECT
    id_t_debug                AS id,
    TO_CHAR(timestamp_debug, 'DD/MM/YYYY HH24:MI:SS') AS timestamp,
    description_debug         AS descrizione
FROM
    t_debug
order by timestamp_debug desc;
/

-- INSERT INTO DEBUG TABLE VERSION 2

CREATE OR REPLACE PROCEDURE p_debug2 (
    description_debug VARCHAR2
) IS
    timestamp_debug   DATE;
    id_debug          NUMBER;
BEGIN
    timestamp_debug := SYSDATE;
    id_debug := t_debug_seq.nextval;
    BEGIN
    INSERT INTO t_debug VALUES (
        id_t_debug,
        timestamp_debug,
        description_debug
    );
    COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line('RISCONTRATO ERRORE GENERICO');
        ROLLBACK;
    END;
EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line('RISCONTRATO ERRORE GENERICO');
END;
/

--CURSOR FOR LOOP

BEGIN
  FOR S in (SELECT * FROM employees)
  LOOP
    -- process current record of S
  END LOOP;
END;
/

--CREATE FOREIGN KEY IN A TABLE

CREATE TABLE table_name
(
  column1 datatype null/not null,
  column2 datatype null/not null,
  ...

  CONSTRAINT fk_column
    FOREIGN KEY (column1, column2, ... column_n)
    REFERENCES parent_table (column1, column2, ... column_n)
);

--EXEMPLE 

CREATE TABLE supplier
( supplier_id numeric(10) not null,
  supplier_name varchar2(50) not null,
  contact_name varchar2(50),
  CONSTRAINT supplier_pk PRIMARY KEY (supplier_id)
);

CREATE TABLE products
( product_id numeric(10) not null,
  supplier_id numeric(10) not null,
  CONSTRAINT fk_supplier
    FOREIGN KEY (supplier_id)
    REFERENCES supplier(supplier_id)
);
/

--ALTER TABLE ADD CONSTRAINT

ALTER TABLE
   EMPLOYEES
ADD CONSTRAINT
   EMPLOYEE_ID FOREIGN KEY (EMPLOYEE_ID)
REFERENCES
   EMPLOYEES (EMPLOYEE_ID)
INITIALLY DEFERRED DEFERRABLE;
/

	--CONSTRAINT DEFERRED
	CREATE TABLE tab1 (id  NUMBER(10), tab2_id NUMBER(10));
	CREATE TABLE tab2 (id  NUMBER(10));
	/

	ALTER TABLE tab2 ADD PRIMARY KEY (id);
	/

	ALTER TABLE tab1 ADD CONSTRAINT fk_tab1_tab2
	  FOREIGN KEY (tab2_id)
	  REFERENCES tab2 (id)
	  DEFERRABLE
	  INITIALLY IMMEDIATE;
	/

	ALTER SESSION SET CONSTRAINTS = DEFERRED;
	ALTER SESSION SET CONSTRAINTS = IMMEDIATE;
	/

-- CALL THE PROCEDURE P_DEBUG 	
	CREATE OR REPLACE PROCEDURE p_sqldevdebug (
    p_par1 NUMBER
) IS
    p_par3   NUMBER;
    v_par2   NUMBER;
BEGIN
    p_par3 := p_par1;
    p_debug(t_debug_seq.nextval, SYSDATE, 'p_art1 =' || p_par1 || 'p_art2 =' || v_par2);
    v_par2 := 0;
    p_debug(t_debug_seq.nextval, SYSDATE, 'p_art1 =' || p_par1 || 'p_art2 =' || v_par2);
    dbms_output.put_line('p_par1 -> ' || p_par1);
    dbms_output.put_line('v_par2 -> ' || v_par2);
    p_debug(t_debug_seq.nextval, TO_CHAR(timestamp_debug, 'hh24:mn:ss  DD/MM/YYY'), 'success');
EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line('RISCONTRATO ERRORE GENERICO');
END;

select * from table(dbms_xplan.display_cursor(sql_id=>'5phg44gyjm6v6', format=>'ALLSTATS LAST'));

	--The ALTER SESSION... statements show how the state of the constraint can be changed. 
	--These ALTER SESSION... statements will not work for constraints that are created as NOT DEFERRABLE.
	
--TYPE nom IS VARRAY(longueur) OF type_des_valeurs----> dichiaration des VARRAY IN PL/SQL

 TYPE user_role_privs_va IS VARRAY(10) OF VARCHAR2(30);
/

DECLARE
  CURSOR mesbudgets IS
   SELECT * FROM BUDGET
   WHERE EXERCICE = '2007';
  mon_budget BUDGET%ROWTYPE;
BEGIN
  OPEN mesbudgets;
  LOOP
    FETCH mesbudgets INTO mon_budget;
    DBMS_OUTPUT.PUT_LINE(mon_budget.DORIS_KEY);
    EXIT WHEN mesbudgets%NOTFOUND;
  END LOOP;
  CLOSE mesbudgets;
END;
/

create or replace PROCEDURE employee_name IS
    CURSOR cur_empl_name IS
    SELECT
        first_name
    FROM
        employees;

    empl_name VARCHAR2(512);
BEGIN
    OPEN cur_empl_name;
    LOOP
        FETCH cur_empl_name INTO empl_name;
        dbms_output.put_line(empl_name);
        EXIT WHEN cur_empl_name%notfound;
    END LOOP;

    CLOSE cur_empl_name;
END;
/

--CREATE A TABLE FROM AN EXISTING TABLE 

create table employees_doppi as SELECT *  from employees;
/

-- RECORD DOPPI 

SELECT(OR DELETE)
    *
FROM
    employees_doppi table1_alias1
WHERE
    EXISTS (
        SELECT
            1
        FROM
            employees_doppi table1_alias2
        WHERE
            table1_alias1.first_name = table1_alias2.first_name
            AND table1_alias1.rowid < table1_alias2.rowid
    )
ORDER BY
   table1_alias1.first_name;
/

--INSERT INTO TABLE 

INSERT INTO employees_doppi (
    employee_id,
    first_name,
    last_name,
    email,
    phone_number,
    hire_date,
    job_id,
    salary,
    commission_pct,
    manager_id,
    department_id
) VALUES (
    '111',
    'Ismael',
    'Sciarra',
    'ISCIARRA',
    '515.124.4369',
    TO_DATE('30-SET-05', 'DD-MON-RR'),
    'FI_ACCOUNT',
    '7700',
    NULL,
    '108',
    '100'
);

COMMIT;
/

GRANT DEBUG CONNECT SESSION TO "SCOTT" ;
GRANT DEBUG ANY PROCEDURE TO "SCOTT" ;
/

--PROCEDURE PER INSERT INFORMATIONS DENTRO LA TABELLA DI DEBUG
create or replace PROCEDURE p_debug (
    id_t_debug          NUMBER,
    timestamp_debug     DATE,
    description_debug   VARCHAR2
) IS
BEGIN
    INSERT INTO t_debug VALUES (
        id_t_debug,
        timestamp_debug,
        description_debug
    );
    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line('RISCONTRATO ERRORE GENERICO');
END;
/

--Left join

	SELECT <column_name>, <column_name>
FROM <table_name alias>, <table_name alias>
WHERE <alias.column_name = <alias.column_name>
AND <alias.column_name> = <alias.column_name> (+);

--exemple
SELECT p.last_name, t.title_name
FROM person p, title t
WHERE p.title_1 = t.title_abbrev(+);
/

--Right join
SELECT <column_name>, <column_name>
FROM <table_name alias>, <table_name alias>
WHERE <alias.column_name> (+) = <alias.column_name

--EXEMPLE
SELECT p.last_name, t.title_name
FROM person p, title t
WHERE p.title_1(+) = t.title_abbrev;
/
--UPDATE MASSIVE: PRIMA SI FA LA SELECT DI CIO' CHE SI VUOLE MODIFICRE POI SI 
--FA LA UPDATE  SUL CAMPO SELEZIONATO 

   
UPDATE employees_doppi table1_alias1
SET
    first_name = 'MICHAEL'
WHERE
    EXISTS (
        SELECT
            1
        FROM
            employees_doppi table1_alias2
        WHERE
            table1_alias1.first_name = table1_alias2.first_name
            AND table1_alias1.rowid < table1_alias2.rowid
    );
/

--il comando MERGE, grazie al quale è possibile effettuare tutta una serie di operazioni contemporaneamente, 
--come l'aggiornamento e l'inserimento, ma anche la cancellazione.
--Procediamo con un esempio:


MERGE INTO test1 a
USING all_objects b
ON (a.object_id = b.object_id)
WHEN MATCHED THEN
UPDATE SET a.status = b.status
WHEN NOT MATCHED THEN
INSERT (object_id, status)
VALUES (b.object_id, b.status);
/

-- MERGE: 

MERGE INTO test1 a USING all_objects b ON ( a.object_id = b.object_id )
WHEN MATCHED THEN UPDATE SET a.status = b.status
WHEN NOT MATCHED THEN INSERT (
    object_id,
    status ) VALUES (
    b.object_id,
    b.status );
/

CREATE TABLE test1
    AS
        SELECT
            b.object_id,
            b.status
        FROM
            all_objects b
/

SELECT * FROM test1 order by object_id;
ROLLBACK
/
DELETE test1 where (object_id = 100);

SELECT COUNT(*) FROM TEST1;

select * from test1 where status != 'VALID';

update test1 set status = 'cicio' where object_id =  116;
ROLLBACK;
commit;
/

--  WITH

SELECT e.ename AS employee_name,
       dc.dept_count AS emp_dept_count
FROM   emp e,
       (SELECT deptno, COUNT(*) AS dept_count
        FROM   emp
        GROUP BY deptno) dc
WHERE  e.deptno = dc.deptno;
/

select * from emp;
					
--Utilizzando la clausola WITH:

WITH dept_count AS (
  SELECT deptno, COUNT(*) AS dept_count
  FROM   emp
  GROUP BY deptno)
SELECT e.ename AS employee_name,
       dc.dept_count AS emp_dept_count
FROM   emp e,
       dept_count dc
WHERE  e.deptno = dc.deptno;

-- Domande 


--- le relazioni molti a molti non sono gestite in sql . per farlo devo utilizzare una tabella di transcodifica. 
-- quella tabella contiene l'id di riferimente alle due tabelle.
  
-- table constraint è un vincolo tra due tabella. eg: foreign key 
-- column constraint è un vincolo su una colonna. eg : una colonna non puo essere nulla.

-- delete è un'istruzione che appartiene alla famiglia delle DML che permette di cancellare una o piu righe di una tabella. 
-- dopo una delete si può fare rollback e recuperare i dati.

-- a diferenza della delete, la truncate permete di cancellare i dati da una tabella ma non c'è verso cioè dopo 
-- una truncate non si può fare una rollback.

-- le caratteristiche di una tranzazione sono: Autonomo, consistenti, isolation e durabile
-- Nell'ambito dei database, ACID deriva dall'acronimo inglese Atomicity, Consistency, Isolation, 
-- and Durability (Atomicità, Consistenza, Isolamento e Durabilità).

-- Nell'ambito dei database, ACID deriva dall'acronimo inglese Atomicity, Consistency, Isolation, e Durability (Atomicità, 
--Consistenza, Isolamento e Durabilità)

-- Nella teoria delle basi di dati, il termine transazione indica una qualunque sequenza di operazioni lecite che, se eseguita 
-- in modo corretto, produce una variazione nello stato di in una base di dati. In caso di successo, il risultato delle operazioni
-- deve essere permanente o persistente, mentre in caso di insuccesso si deve tornare allo stato precedente all'inizio della transazione. 
-- Le transazioni devono avere le seguenti proprietà logiche: Atomicity, Consistency, Isolation, e Durability (in acronimo ACID).

-- una vista è una query fatta bene 

-- il blocco anonimo non viene salvato nel db mentre la stored procedure viene salvato nel db(è un oggetto del database).

-- la difference tra function e stored procedure è che la function deve restituire un vale mentre 
-- la stored procedure può restituire un valore. 

-- la RAISE  viene utilizzata per sollevare un'eccezione personalizzata.

-- EXECUTE IMMEDIATE viene utilizzato per eseguire statements in maniera dinamica

-- il cursore implicito è gestito da pl/sql  

-- Hint è una forzatura del comportamento di un codice 

-- al netto di avere le due grant di debugg , posso fare la tabella di debugg con sequence o usare le dbms_output

--CRON è uno schedulatore in linux 

--Both UNION and UNION ALL concatenate the result of two different SQLs. They differ in the way they handle duplicates.

--UNION performs a DISTINCT on the result set, eliminating any duplicate rows.
--UNION ALL does not remove duplicates, and it therefore faster than UNION.

-- link per i package 

https://docs.oracle.com/cd/E11882_01/appdev.112/e25519/packages.htm#LNPLS99923

-- CHIAMATA DI UN METODO DI UN PACKAGE . IL PACKAGE DI ESEMPIO FA PARTE DELL'UTENTE SCOTT

DECLARE
  EMPNO NUMBER;
BEGIN
  EMPNO := 5;

  EMP_ACTIONS.fire_employee(
    emp_id  => EMPNO 
  );
--rollback; 
END;
/
--SECOND METODO
DECLARE
  EMPNO NUMBER;
BEGIN
  EMPNO := 5;

  EMP_ACTIONS.fire_employee(
     EMPNO 
  );
--rollback; 
END;

-- vista materializzate porta i dati , la vista non porta i dati.
-- la vista materializzata a differenza delle viste porta anche i dati.le viste materializzate si aggiornano sotto richiesta 
--mentre la vista si modifica automaticamento quando quando modifichiamo la tabella a sehuito di una DML.

-- PIVOT: EXEMPLE CREAZIONE TABELLA DI PROVA E CREAZIONE DEL PIVOT 

DECLARE
  EMPNO NUMBER;
BEGIN
  EMPNO := 5;

  EMP_ACTIONS.fire_employee(
     EMPNO 
  );
--rollback; 
END;
/

CREATE TABLE pivot_test (
  id            NUMBER,
  customer_id   NUMBER,
  product_code  VARCHAR2(5),
  quantity      NUMBER
);
/


INSERT INTO pivot_test VALUES (1, 1, 'A', 10);
INSERT INTO pivot_test VALUES (2, 1, 'B', 20);
INSERT INTO pivot_test VALUES (3, 1, 'C', 30);
INSERT INTO pivot_test VALUES (4, 2, 'A', 40);
INSERT INTO pivot_test VALUES (5, 2, 'C', 50);
INSERT INTO pivot_test VALUES (6, 3, 'A', 60);
INSERT INTO pivot_test VALUES (7, 3, 'B', 70);
INSERT INTO pivot_test VALUES (8, 3, 'C', 80);
INSERT INTO pivot_test VALUES (9, 3, 'D', 90);
INSERT INTO pivot_test VALUES (10, 4, 'A', 100);
COMMIT;
/

SELECT * FROM pivot_test;
/

SELECT *
FROM   (SELECT product_code, quantity
        FROM   pivot_test)
PIVOT  (SUM(quantity) AS sum_quantity FOR (product_code) IN ('A' AS a, 'B' AS b, 'C' AS c));
/

SELECT *
FROM   (SELECT customer_id, product_code, quantity
        FROM   pivot_test)
PIVOT  (SUM(quantity) AS sum_quantity FOR (product_code) IN ('A' AS a, 'B' AS b, 'C' AS c))
ORDER BY customer_id;/

SELECT *
FROM   (SELECT product_code, quantity
        FROM   pivot_test)
PIVOT XML (SUM(quantity) AS sum_quantity FOR (product_code) IN (SELECT DISTINCT product_code 
                                                                FROM   pivot_test
                                                                WHERE  id < 10));
/

--Un indice è un oggetto schema che contiene una voce per ciascun valore che appare nelle colonne indicizzate della tabella o 
--del cluster e fornisce un accesso diretto e veloce alle righe.

-- 										GLI AMBIENTI DI STRUTTURAZIONE DEL PROGETTO
--ambiente di sviluppo,di consolidamento,certificazione e di produzione. i primi due sono più funzionali allo sviluppo mentre la cerficazione viene 
--utilizzata ai fini della validazione lato cliente e la produzione infine contradistingue il prodotto una volta rilasciato

-- Differenza tra Prepared Statement, Statememt and CallableStatement
-- * Statement is slower while prepared statement is faster. In most of time, Statement interface is used for DDL statements like CREATE, ALTER, DROP 
-- * Statement will be used for executing static SQL statements and it can't accept input parameters. 
-- * PreparedStatement will be used for executing SQL statements many times dynamically. It will accept input parameters.
-- * CallableStatement is Used to execute the stored procedures.

